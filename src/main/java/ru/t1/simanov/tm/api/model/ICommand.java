package ru.t1.simanov.tm.api.model;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    String toString();

    void execute();

}
