package ru.t1.simanov.tm.command.system;

import ru.t1.simanov.tm.command.AbstractCommand;
import ru.t1.simanov.tm.enumerated.Role;

public final class TestCommand extends AbstractCommand {

    @Override
    public void execute() {
        System.out.println("TEST!");
    }

    @Override
    public String getArgument() {
        return "-t";
    }

    @Override
    public String getDescription() {
        return "TEST COMMAND";
    }

    @Override
    public String getName() {
        return "test";
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
